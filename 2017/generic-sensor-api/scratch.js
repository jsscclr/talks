// No need to feature detect thanks to try...catch block.
try {
  let sensor = new GeolocationSensor()

  sensor.start()
  sensor.onerror = error => gracefullyDegrade(error)
  sensor.onreading = data => updatePosition(sensor.latitude, sensor.longitude)
} catch (error) {
  gracefullyDegrade(error)
}

navigator.geolocation.getCurrentPosition(handlePositionCallback)

function handlePositionCallback(position) {
  console.info(`
    Latitude: ${position.coords.latitude}
    Longitude: ${position.coords.longitude}
    Accuracy: ${position.coords.accuracy}
          
    Altitude: ${position.coords.altitude}
    Altitude Accuracy: ${position.coords.altitudeAccuracy}

    Heading: ${position.coords.heading}
    Speed: ${position.coords.speed}
  `)
}
