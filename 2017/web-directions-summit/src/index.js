import "./variables.css"
import "./reset.css"
import "./index.css"
import "./slide.css"
import "./slide-code.css"
import "./slide-site.css"
import "./slide-quote.css"
import "./slide-list.css"
import "./slide-concept.css"
import "./svg.css"
import "./slide-grid.css"
import "../assets/coinhive-icon.png"
import "../assets/wpclogo_qrcode.png"
import Worker from "./test.worker.js"

const observer = new IntersectionObserver(onSlideVisible, {
  threshold: 1.0,
})

const slides = document.querySelectorAll(".slide")

for (let slide of slides) {
  observer.observe(slide)
}

function onSlideVisible(entries) {
  entries.forEach(entry => {
    if (entry.intersectionRatio === 1) {
      entry.target.classList.add("slide--visible")
    } else {
      entry.target.classList.remove("slide--visible")
    }
  })
}

const worker = new Worker()

worker.onmessage = event => {
  console.log("message received")
  console.log(event)
}

worker.onmessageerror = error => {
  console.log("error")
  console.log(error)
}

worker.postMessage("hello")

console.log("set-up worker")
console.log(worker)
