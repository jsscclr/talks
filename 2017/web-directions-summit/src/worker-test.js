const worker = new Worker("./worker.js")

worker.onmessage = event => {
  console.log("message received")
  console.log(event)
}

worker.onmessageerror = error => {
  console.log("error")
  console.log(error)
}

worker.postMessage("hello")

console.log("set-up worker")
console.log(worker)
