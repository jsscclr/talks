/* global require, __dirname, module */
const { resolve } = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
  devtool: "cheap-module-eval-source-map",
  devServer: {
    contentBase: resolve(__dirname, "assets"),
    historyApiFallback: true,
    hot: true,
    publicPath: "/",
  },
  context: resolve(__dirname, "src"),
  entry: {
    app: [
      "webpack-dev-server/client?http://localhost:8080",
      "webpack/hot/only-dev-server",
      resolve(__dirname, "src", "index.js"),
    ],
  },
  module: {
    rules: [
      {
        include: resolve(__dirname, "src"),
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },

      {
        include: resolve(__dirname, "assets"),
        test: /\.(png|jpg)$/,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
      {
        test: /\.worker\.js$/,
        use: { loader: "worker-loader" },
        include: resolve(__dirname, "src"),
      },
    ],
  },
  output: {
    filename: "[name].bundle.js",
    chunkFilename: "[name].bundle.js",
    path: resolve(__dirname, "dist"),
    publicPath: "/",
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Web Directions Summit",
      template: resolve(__dirname, "src", "index.html"),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
}
