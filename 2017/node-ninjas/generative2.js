const width = 82
const height = 100
const size = width * height

function generate(seed) {
  const symbols = [
    "¦",
    "■",
    "▒",
    "‗",
    "≡",
    "¨",
    "¯",
    "_",
    "←",
    "✚",
    "■",
    "□",
    "▬",
    "▮",
    "▭",
    "▯" 
  ]
  const stringParts = []

  for (let i = 0; i < size; i++) {
    stringParts.push(  
      symbols[Math.round(Math.random() * seed % symbols.length)]
    )
  }

  return stringParts.join("");
}

const result = generate(process.argv[2])

console.log(result)
