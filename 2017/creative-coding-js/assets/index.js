const observer = new IntersectionObserver(onSlideVisible, { threshold: 1.0 })
const slides = document.querySelectorAll(".slide")

for (let slide of slides) {
  observer.observe(slide)
}

function onSlideVisible(entries) {
  entries.forEach(entry => {
    if (entry.intersectionRatio === 1) {
      entry.target.classList.add("slide--visible")
    } else {
      entry.target.classList.remove("slide--visible")
    }
  })
}
